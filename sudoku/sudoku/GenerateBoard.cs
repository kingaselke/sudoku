﻿namespace sudoku
{
    class GenerateBoard
    {
        public int[,] BoardGenerate()
        {
            int[,] tab = new int[9, 9];
            int number = 1;
            for (int j = 0; j < 9; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    tab[i, j] = number;
                    number++;
                    if (number > 9) { number = 1; }
                }
                if (j == 0)
                {
                    number = 4;
                }
                if (j == 1)
                {
                    number = 7;
                }
                if (j == 2)
                {
                    number = 9;
                }
                if (j == 3)
                {
                    number = 3;
                }
                if (j == 4)
                {
                    number = 6;
                }
                if (j == 5)
                {
                    number = 8;
                }
                if (j == 6)
                {
                    number = 2;
                }
                if (j == 7)
                {
                    number = 5;
                }
            
            }
            return tab;
        }
    }
}
