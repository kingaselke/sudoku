﻿using System.Diagnostics;
using PdfSharp.Drawing;
using PdfSharp.Pdf;

namespace sudoku
{
    public class GeneratePdf
    {
        public void Pdf(string[,] tab)
        {
            var document = new PdfDocument();

            // Create an empty page
            PdfPage page = document.AddPage();

            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);

            double fontHeight = 36;
            XFont font = new XFont("Times New Roman", fontHeight, XFontStyle.BoldItalic);

            // Get the centre of the page
            double y = page.Height / 4;

            XPen pen = new XPen(XColors.Black, 4);


            gfx.DrawLine(pen, y + 20, 250, y + 20, 700);
            gfx.DrawLine(pen, y + 160, 250, y + 160, 700);
            gfx.DrawLine(pen, 100, 410, 500, 410);
            gfx.DrawLine(pen, 100, 560, 500, 560);

            XRect rect = new XRect(0, y, page.Width, fontHeight);
            for (int j = 0; j < 9; j++)
            {
                string tabLine = "";
                for (int i = 0; i < 9; i++)
                {
                    if (i == 2 || i == 5)
                        tabLine = tabLine + " " + tab[i, j] + "   ";
                    else
                        tabLine = tabLine + tab[i, j];
                }
                y = y + 50;
                rect = new XRect(0, y, page.Width, fontHeight);
                gfx.DrawString(tabLine, font,
                    XBrushes.Black, rect, XStringFormats.Center);
            }

            // Save the document...
            string filename = "Sudokuk.pdf";
            document.Save(filename);
            // ...and start a viewer.
            Process.Start(filename);
        }


    }
}
