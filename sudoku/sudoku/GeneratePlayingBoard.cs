﻿using System;
using System.Threading;

namespace sudoku
{
   public class GeneratePlayingBoard
    {
        public  string[,] PlayingTab(int[,] tab, int level)
        {
            string[,] playingTab = new string[9, 9];

            int count = 0;
            for (int j = 0; j < 9; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    playingTab[i, j] = " _ ";

                }
            }
           while (count < level)
            {
                var randomI = new Random().Next(0, 9);
                Thread.Sleep(10);
                var randomJ = new Random().Next(0, 9);
                Thread.Sleep(10);

                if (playingTab[randomI, randomJ] == " _ ")
                {
                    playingTab[randomI, randomJ] = " " + tab[randomI, randomJ] + " ";
                    count++;
                }
           }

            return playingTab;
        }
        
        public  int[,] RandomingBoard()
        {
            var generateBoard = new GenerateBoard();
            var tab = generateBoard.BoardGenerate();

            for (int i = 0; i < 9; i++)
            {
                if (i % 3 == 0)
                {
                    var random = new Random().Next(i, i + 3);
                    var random2 = new Random().Next(i, i + 3);

                    while (true)
                    {
                        if (random == random2)
                        {
                            random2 = new Random().Next(i, i + 3);
                        }
                        else
                        {
                            break;
                        }
                    }
                    
                    for (int j = 0; j < 9; j++)
                    {
                        var temp = tab[random, j];
                        tab[random, j] = tab[random2, j];
                        tab[random2, j] = temp;
                    }
                }
            }

            for (int i = 0; i < 9; i++)
            {
                if (i % 3 == 0)
                {
                    var random = new Random().Next(i, i + 3);
                    var random2 = new Random().Next(i, i + 3);

                    while (true)
                    {
                        if (random == random2)
                        {
                            random2 = new Random().Next(i, i + 3);
                        }

                        else
                        {
                            break;
                        }
                    }

                    for (int j = 0; j < 9; j++)
                    {
                        var temp = tab[j, random];
                        tab[j, random] = tab[j, random2];
                        tab[j, random2] = temp;
                    }
                }
            }

            for (int j = 0; j < 9; j++)
            {
                var temp = tab[j, 0];
                tab[j, 0] = tab[j, 6];
                tab[j, 6] = temp;

                var temp1 = tab[j, 1];
                tab[j, 1] = tab[j, 7];
                tab[j, 7] = temp1;

                var temp2 = tab[j, 2];
                tab[j, 2] = tab[j, 8];
                tab[j, 8] = temp2;
            }

            return tab;
        }
    }
}
