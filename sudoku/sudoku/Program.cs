﻿using System;

namespace sudoku
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var generatePdf = new GeneratePdf();
            var generatePlayingBoard = new GeneratePlayingBoard();
            var tab = generatePlayingBoard.RandomingBoard();
            var level = PickUpLevel();

            PrintTab(generatePlayingBoard.PlayingTab(tab, level));
            generatePdf.Pdf(generatePlayingBoard.PlayingTab(tab, level));
        }

        public static int PickUpLevel()
        {
            int level=0;
            while (true)
            {
                 Console.WriteLine("Get the level : \n 1. Easy \n 2. Midium \n 3. Hard");
                var command = Int32.Parse(Console.ReadLine());
                
                switch (command)
                {
                    case 1:
                        level = 40;
                        break;
                    case 2:
                        level = 35;
                        break;
                    case 3:
                        level = 30;
                        break;
                    default:
                        Console.WriteLine("Level not found!");
                        break;
                }

                if (command<=3)
                    break;
            }
           return level;
        }

        public static void PrintTab(string[,] tab)
        {
            for (int j = 0; j < 9; j++)
            {

                for (int i = 0; i < 9; i++)
                {
                    if (i == 2 || i == 5)
                    {
                        Console.Write(tab[i, j] + " | ");
                    }

                    else
                        Console.Write(tab[i, j] + "   ");
                }

                if (j == 2 || j == 5)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("--------------------------------------------------");
                }
                else
                    Console.WriteLine("\n");
            }

        }
    }
}
